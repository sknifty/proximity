package com.headlightsolutions.ro.proximity;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.IStatusCallback;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.BleSignal;
import com.google.android.gms.nearby.messages.Distance;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.StatusCallback;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeCallback;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.headlightsolutions.ro.proximity.requests.LocationRequest;
import com.headlightsolutions.ro.proximity.requests.MessageRequest;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static com.google.android.gms.nearby.messages.Strategy.DISTANCE_TYPE_EARSHOT;

public class ProximityService extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private PowerManager.WakeLock wakelock;
    private static final String TAG = ProximityService.class.getSimpleName();
    private Message mPubMessage;
    private MessageListener mMessageListener;
    private static final int TTL_IN_SECONDS = 20;
    private static final int TTL_IN_SECONDS_FOR_SUB = 60;
    private static Map<String,Date> neighbors = new HashMap<String,Date>();

    public static boolean WithEarShot = true;


    //private static final Strategy SUB_PUB_STRATEGY = new Strategy.Builder().setDistanceType(DISTANCE_TYPE_EARSHOT).setTtlSeconds(TTL_IN_SECONDS).build();
    private static final Strategy PUB_STRATEGY_WITH_EARSHOT = new Strategy.Builder().setDistanceType(DISTANCE_TYPE_EARSHOT).setTtlSeconds(TTL_IN_SECONDS).build();
    private static final Strategy SUB_STRATEGY_WITH_EARSHOT = new Strategy.Builder().setDistanceType(DISTANCE_TYPE_EARSHOT).setTtlSeconds(TTL_IN_SECONDS).build();

    private static final Strategy PUB_STRATEGY = new Strategy.Builder().setTtlSeconds(TTL_IN_SECONDS).build();
    private static final Strategy SUB_STRATEGY = new Strategy.Builder().setTtlSeconds(TTL_IN_SECONDS).build();
    //private static final Strategy SUB_STRATEGY = new Strategy.Builder().setDistanceType(DISTANCE_TYPE_EARSHOT).setTtlSeconds(Strategy.TTL_SECONDS_INFINITE).build();

    //private static final Strategy PUB_STRATEGY = new Strategy.Builder().setTtlSeconds(TTL_IN_SECONDS).build();
    //private static final Strategy SUB_STRATEGY = new Strategy.Builder().setTtlSeconds(TTL_IN_SECONDS).build();

    //private static final Strategy PUB_STRATEGY = (new Strategy.Builder()).zze(2).setTtlSeconds(TTL_IN_SECONDS).build();
    //private static final Strategy SUB_STRATEGY = Strategy.BLE_ONLY;

    private GoogleApiClient mGoogleApiClient;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        InitializeLocationManager();

        PowerManager pmanager = (PowerManager) getSystemService(Context.POWER_SERVICE);

        wakelock = pmanager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Proximity:servicewakelock");
        wakelock.setReferenceCounted(false);
        wakelock.acquire();

        mMessageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                String gasit = new String(message.getContent()).trim();
                if (!neighbors.containsKey(gasit)) {
                    neighbors.put(gasit, Calendar.getInstance().getTime());
                    String location = "";
                    if (currentLocation != null) {
                        location = currentLocation.getLatitude() + " " + currentLocation.getLongitude();
                    }
                    try {
                        String str_result = new MessageRequest().execute(myId, gasit, "1", location).get();
                        JSONObject response = new JSONObject(str_result);
                        if (response.getBoolean("status")) {
                            JSONObject currentUser = response.getJSONObject("content");
                            createNotification("Found: " + currentUser.getString("full_name"), location);
                        }

                    } catch (InterruptedException | ExecutionException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    neighbors.put(gasit, Calendar.getInstance().getTime());
                }
            }

            @Override
            public void onDistanceChanged(final Message message, final Distance distance) {
                Log.i(TAG, "Distance changed, message: " + message + ", new distance: " + distance);
            }
//
            @Override
            public void onBleSignalChanged(final Message message, final BleSignal bleSignal) {
                Log.i(TAG, "Message: " + message + " has new BLE signal information: " + bleSignal);
            }

            @Override
            public void onLost(final Message message) {
                String gasit = new String(message.getContent()).trim();
                Log.i(TAG, "Lost: " + gasit);
//                String gasit = new String(message.getContent()).trim();
//                String location = "";
//                if (currentLocation != null) {
//                    location = currentLocation.getLatitude() + " " + currentLocation.getLongitude();
//                }
//                try {
//                    String str_result = new MessageRequest().execute(myId, gasit, "0", location).get();
//                } catch (InterruptedException | ExecutionException e) {
//                    e.printStackTrace();
//                }
            }
        };


        //buildGoogleApiClient();

        //startTimer();
    }

    private void buildGoogleApiClient() {
        if (mGoogleApiClient != null) {
            return;
        }
        //MainActivity.instance
        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.instance)
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "GoogleApiClient suspended");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient suspended");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
       // publish();
       // subscribe();
      //  startTimer();
    }


    private void subscribe() {
        Log.i(TAG, "Subscribing WithEarshot=" + WithEarShot);

        Strategy aux_sub_strat = WithEarShot?SUB_STRATEGY_WITH_EARSHOT:SUB_STRATEGY;

        SubscribeOptions subscribeOptions = new SubscribeOptions.Builder()
                .setStrategy(aux_sub_strat)
                .setCallback(new CustomSubscribeCallBack()).build();

        Nearby.getMessagesClient(MainActivity.instance).subscribe(mMessageListener, subscribeOptions)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        Log.i(TAG, "Subscribed successfully.");
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i(TAG, "Subscribed failure.");
                    }
                });


//        Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, subscribeOptions)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        if (status.isSuccess()) {
//                            Log.i(TAG, "Subscribed successfully.");
//                        } else {
//                            Log.i(TAG, "Could not subscribe, status = " + status);
//                        }
//                    }
//                });
    }

    private void publish() {
        Log.i(TAG, "Publishing WithEarshot=" + WithEarShot);

        Strategy aux_pub_strat = WithEarShot?PUB_STRATEGY_WITH_EARSHOT:PUB_STRATEGY;

        PublishOptions publishOptions = new PublishOptions.Builder()
                .setStrategy(aux_pub_strat)
                .setCallback(new CustomPublishCallBack()).build();

        Nearby.getMessagesClient(MainActivity.instance).publish(mPubMessage, publishOptions)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        Log.i(TAG, "Published successfully.");
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG, "Published failure.");
            }
        });

//        Nearby.Messages.publish(mGoogleApiClient, mPubMessage, publishOptions)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        if (status.isSuccess()) {
//                            Log.i(TAG, "Published successfully.");
//                        } else {
//                            //publishBLEOnly();
//                            Log.i(TAG, "Could not publish, status = " + status);
//                        }
//                    }
//                });
    }

    private void unsubscribe() {
        Log.i(TAG, "Unsubscribing.");
        Nearby.getMessagesClient(MainActivity.instance).unsubscribe(mMessageListener)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        Log.i(TAG, "Unsubscribed successfully.");
                        //subscribe();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG, "Unsubscribed failure.");
            }
        });
//        Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener).setResultCallback(new ResultCallback<Status>() {
//            @Override
//            public void onResult(@NonNull Status status) {
//                if (status.isSuccess()) {
//                    Log.i(TAG, "Unsubscribed successfully.");
//                    //subscribe();
//                } else {
//                    Log.i(TAG, "Could not Unsubscribed, status = " + status);
//                }
//            }
//        });
    }

    private void unpublish() {
        Log.i(TAG, "Unpublishing.");
        Nearby.getMessagesClient(MainActivity.instance).unpublish(mPubMessage)
                .addOnSuccessListener(new OnSuccessListener() {
                    @Override
                    public void onSuccess(Object o) {
                        Log.i(TAG, "Unpublished successfully.");
                        //publish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i(TAG, "Unpublished failure.");
            }
        });
//        Nearby.Messages.unpublish(mGoogleApiClient, mPubMessage)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        if (status.isSuccess()) {
//                            Log.i(TAG, "Unpublished successfully.");
//                            //publish();
//                        } else {
//                            Log.i(TAG, "Could not Unpublished, status = " + status);
//                        }
//                    }
//                });
    }

    class CustomPublishCallBack extends PublishCallback {

        @Override
        public void onExpired() {

            unpublish();
        }
    }

    class CustomSubscribeCallBack extends SubscribeCallback {
        @Override
        public void onExpired() {

            unsubscribe();
        }
    }

    private NotificationManager notif_manager;
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            notif_manager = getSystemService(NotificationManager.class);
            notif_manager.createNotificationChannel(serviceChannel);
        }
    }


    private String myId;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Pornesc serviciul:");
        myId = intent.getAction();

        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Guvernul României")
                .setContentText("Proximity")
                .setSmallIcon(R.mipmap.icon)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);

        mPubMessage = new Message(myId.getBytes(Charset.forName("UTF-8")));
        publish();
        subscribe();
        startTimer();

        //boolean xx = mGoogleApiClient.isConnecting();
        //publish();
        //subscribe();
        //return START_NOT_STICKY;
        return START_STICKY;
    }

    private LocationManager locationManager;

    private void InitializeLocationManager() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 100, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 100, this);
    }

    private Location currentLocation = null;
    private Location currentLocationForDistance = null;

    List<JSONObject> locationsHistory = new ArrayList<JSONObject>();
    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
        if (currentLocationForDistance == null || currentLocationForDistance.distanceTo(location) > 25) {
            currentLocationForDistance = location;

            try {
                JSONObject historyEntrie =  new JSONObject();
                historyEntrie.put("location", currentLocationForDistance.getLatitude() + " " + currentLocationForDistance.getLongitude() );
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("gmt"));
                String frm = format.format(new Date());

                historyEntrie.put("datetime", frm);
                locationsHistory.add(historyEntrie);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }



    private Timer timer = null;
    private TimerTask timerTask;
    final Handler handler = new Handler();
    public void startTimer() {
        //set a new Timer
        if (timer == null) {
            timer = new Timer();

            //initialize the TimerTask's job
            initializeTimerTask();

            //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
            timer.schedule(timerTask, (TTL_IN_SECONDS + 2) * 1000, (TTL_IN_SECONDS + 5) * 1000); //
        } else {

        }
    }

    int countForResubscribe = 0;

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        FileUtils.deleteQuietly(ProximityService.this.getCacheDir());

                        //if (mGoogleApiClient != null)
                          //  mGoogleApiClient.disconnect();
//                        //mGoogleApiClient.connect();
//                        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.instance)
//                                .addApi(Nearby.MESSAGES_API)
//                                .addConnectionCallbacks(ProximityService.this)
//                                .build();
//
//                        mGoogleApiClient.connect();
                        publish();
                        subscribe();

                        List<String> keysForRemove = new ArrayList<>();
                        Date currentTime = Calendar.getInstance().getTime();
                        for (String key:neighbors.keySet()) {
                            Log.i(TAG, key + " - " + neighbors.get(key).toString());

                            long diffInMs = currentTime.getTime() - neighbors.get(key).getTime();
                            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
                            Log.i(TAG, "diffInSec - " + diffInSec);

                            if (diffInSec > 120) {
                                keysForRemove.add(key);
                                String location = "";
                                if (currentLocation != null) {
                                    location = currentLocation.getLatitude() + " " + currentLocation.getLongitude();
                                }
                                try {
                                    String str_result = new MessageRequest().execute(myId, key, "0", location).get();
                                    JSONObject response = new JSONObject(str_result);
                                    if (response.getBoolean("status")) {
                                        JSONObject currentUser = response.getJSONObject("content");
                                        createNotification("Lost: " + currentUser.getString("full_name"), location);
                                    }
                                } catch (InterruptedException | ExecutionException | JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        for (String key: keysForRemove) {
                            neighbors.remove(key);
                        }
                        keysForRemove.clear();

                        if (locationsHistory.size() > 0) {
                            try {
                                JSONObject data = new JSONObject();
                                data.put("user_id", myId);

                                JSONArray locations = new JSONArray();
                                for (JSONObject historyEntrie : locationsHistory) {
                                    locations.put(historyEntrie);
                                }
                                data.put("locations", locations);
                                String str_result = new LocationRequest().execute(data).get();

                                JSONObject response = new JSONObject(str_result);
                                if (response.getBoolean("status")) {
                                    locationsHistory.clear();
                                }

                            } catch (InterruptedException | ExecutionException | JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                });
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    int notif_id = 2;
    public void createNotification(String title, String subtitle) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(title)
                .setContentText(subtitle)
                .setSmallIcon(R.mipmap.icon)
                .setContentIntent(pendingIntent)
                .build();

        notif_manager.notify(notif_id, notification);
        notif_id++;
    }
}




