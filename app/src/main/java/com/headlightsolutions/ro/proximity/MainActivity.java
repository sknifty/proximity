package com.headlightsolutions.ro.proximity;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;

import android.telephony.TelephonyManager;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.Messages;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;
import com.google.android.material.snackbar.Snackbar;
import com.headlightsolutions.ro.proximity.requests.InsertUpdateRequest;
import com.headlightsolutions.ro.proximity.requests.LoginRequest;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener  {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 111;
    private static final int MY_IGNORE_OPTIMIZATION_REQUEST = 112;

    private boolean canRestart = false;

    private String Imei;
    private void CheckBattery() {
        PowerManager pmanager = (PowerManager)getSystemService(Context.POWER_SERVICE);

        boolean isIgnoringBatteryOptimizations = pmanager.isIgnoringBatteryOptimizations(getPackageName());
        if (!isIgnoringBatteryOptimizations)
        {
            Intent intent = new Intent();

            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, MY_IGNORE_OPTIMIZATION_REQUEST);
        } else {
            InitializePoneData1();
        }
    }



    @Override
    public void onResume()
    {
        super.onResume();
        withEarshot.setChecked(ProximityService.WithEarShot);
        Log.d("tag", "This screen is back");
    }

    @Override
    public void onPause()
    {
        super.onPause();
    }

    private void InitializePoneData1() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            Imei = telephonyManager.getDeviceId();

            //Telephone Number
            if (Imei == null)
            {
                Imei = Utils.getMACAddress("wlan0");
//                Imei = Utils.getMACAddress("eth0");
//                Imei = Utils.getMACAddress("wlan0");
//                Imei = Utils.getMACAddress("eth0");
                //Utils.getIPAddress(true); // IPv4
                //Utils.getIPAddress(false); // IPv6
                //WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                //WifiInfo wInfo = wifiManager.getConnectionInfo();
                //Imei = wInfo.getMacAddress();
            }
        }

        try {
            String str_result= new LoginRequest().execute(Imei).get();
            JSONObject response = new JSONObject(str_result);
            if (response.getBoolean("status")) {
                //Aici pornesc
                currentUser = response.getJSONObject("content");
                StartSendingMessages();
                phoneEdit.setText(currentUser.getString("phone"));
                nameEdit.setText(currentUser.getString("full_name"));
                emailEdit.setText(currentUser.getString("email"));
                //emailEdit.setText(currentUser.getString("email"));
                saveButton.setText("Actualizați Datele");
            } else {
                saveButton.setText("Salvați Datele și Porniți");
            }
        } catch (InterruptedException | ExecutionException | JSONException e) {
            e.printStackTrace();
        }
    }

    private JSONObject currentUser = null;

    private EditText nameEdit;
    private EditText emailEdit;
    private EditText phoneEdit;
    private Button saveButton;
    private CheckBox withEarshot;

    public static MainActivity instance;
    private GoogleApiClient mGoogleApiClient;

    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder()
            .setTtlSeconds(5).build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        nameEdit = findViewById(R.id.nameEdit);
        emailEdit = findViewById(R.id.emailEdit);
        phoneEdit = findViewById(R.id.phoneEdit);
        saveButton = findViewById(R.id.saveButton);
        withEarshot = findViewById(R.id.checkbox);

        instance = this;

        String[] PERMISSIONS = {
                android.Manifest.permission.READ_PHONE_STATE,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
                //android.Manifest.permission.ACCESS_WIFI_STATE
        };

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_ID_MULTIPLE_PERMISSIONS);
        } else {
            CheckBattery();
        }

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(Nearby.MESSAGES_API)
//                .addConnectionCallbacks(this)
//    //            .enableAutoManage(this, this)
//                .build();
//        mGoogleApiClient.connect();
        //Nearby.getMessagesClient(MainActivity.instance).publish(new Message("".tob))


        //mGoogleApiClient.publish(new Message(bytes));
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void logAndShowSnackbar(final String text) {
        Log.w(TAG, text);
        View container = findViewById(R.id.activity_main_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d(TAG, "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                CheckBattery();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == MY_IGNORE_OPTIMIZATION_REQUEST  && resultCode  == RESULT_OK) {

                InitializePoneData1();
            } else {
                this.finishAffinity();
            }
        } catch (Exception ex) {
            this.finishAffinity();
        }
    }

    public void onClickWithEarshotBtn(View view) {
        ProximityService.WithEarShot = withEarshot.isChecked();
    }

    public void onClickSaveBtn(View view) {
        if (phoneEdit.getText().length() != 10 ) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Eroare")
                    .setMessage("Câmpul Telefon obligatoriu")
                    .setPositiveButton("Da", null)
                    .show();
            return;
        }

        try {
            String str_result= new InsertUpdateRequest().execute(Imei, phoneEdit.getText().toString(), nameEdit.getText().toString(), emailEdit.getText().toString()).get();
            JSONObject response = new JSONObject(str_result);
            if (response.getBoolean("status")) {
                if (currentUser == null) {
                    currentUser =response.getJSONObject("content");
                    StartSendingMessages();
                } else {
                    currentUser =response.getJSONObject("content");
                }

                saveButton.setText("Actualizați Datele");
            }
        } catch (InterruptedException | ExecutionException | JSONException e) {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Eroare")
                    .setMessage("Datele nu au putut fi salvate. Verificați conexiunea la internet.")
                    .setPositiveButton("Da", null)
                    .show();
        }
    }

    private void StartSendingMessages() {
        try {
            if (!isMyServiceRunning(ProximityService.class)) {
                canRestart = true;
                Intent intex = new Intent(this, ProximityService.class);
                intex.setAction(currentUser.getString("id"));
                startService(intex);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        String x = "1";
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String x = "1";
    }

//    private class ErrorCheckingCallback implements ResultCallback<Status> {
//        private final String method;
//        private final Runnable runOnSuccess;
//
//        private ErrorCheckingCallback(String method) {
//            this(method, null);
//        }
//
//        private ErrorCheckingCallback(String method, @Nullable Runnable runOnSuccess) {
//            this.method = method;
//            this.runOnSuccess = runOnSuccess;
//        }
//
//        @Override
//        public void onResult(@NonNull Status status) {
//            if (status.isSuccess()) {
//                Log.i(TAG, method + " succeeded.");
//                if (runOnSuccess != null) {
//                    runOnSuccess.run();
//                }
//            } else {
//                // Currently, the only resolvable error is that the device is not opted
//                // in to Nearby. Starting the resolution displays an opt-in dialog.
//                if (status.hasResolution()) {
////                    if (!mResolvingError) {
////                        try {
////                            status.startResolutionForResult(MainActivity.this,
////                                    REQUEST_RESOLVE_ERROR);
////                            mResolvingError = true;
////                        } catch (IntentSender.SendIntentException e) {
////                            showToastAndLog(Log.ERROR, method + " failed with exception: " + e);
////                        }
////                    } else {
////                        // This will be encountered on initial startup because we do
////                        // both publish and subscribe together.  So having a toast while
////                        // resolving dialog is in progress is confusing, so just log it.
////                        Log.i(TAG, method + " failed with status: " + status
////                                + " while resolving error.");
////                    }
//                } else {
////                    showToastAndLog(Log.ERROR, method + " failed with : " + status
////                            + " resolving error: " + mResolvingError);
//                }
//            }
//        }
//    }
}

