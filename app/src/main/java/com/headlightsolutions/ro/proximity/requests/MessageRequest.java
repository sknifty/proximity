package com.headlightsolutions.ro.proximity.requests;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MessageRequest  extends AsyncTask<String, Void, String> {

    private static final String LOG_TAG = "LoginRequest" ;

    @Override
    protected String doInBackground(String... voids) {
        //String address = "https://hcf.dcti.ro:11443/api/send.message";
        String address = "http://www.headlightsolutions.ro:9001/api/send.messages";
        HttpURLConnection urlConnection;

        try {
            URL url = new URL(address);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");

            JSONObject data  = new JSONObject();
            data.put("user_id", voids[0]);

            JSONObject  message = new JSONObject();
            message.put("neighbor_id", voids[1]);
            message.put("found", voids[2]);
            message.put("location", voids[3]);

            JSONArray messages = new JSONArray();
            messages.put(message);
            data.put("messages", messages);


            OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
            wr.write(data.toString());
            wr.flush();

            InputStream inputStream;
            // get stream
            if (urlConnection.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            // parse stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String temp, response = "";
            while ((temp = bufferedReader.readLine()) != null) {
                response += temp;
            }
            // put into JSONObject
            //JSONObject responseJson = new JSONObject(response);

            return response;
        } catch (IOException | JSONException e) {
            return e.toString();
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Log.i(LOG_TAG, "POST\n" + result);
    }
}
